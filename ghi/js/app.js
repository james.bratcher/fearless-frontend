function createCard(name, location, description, pictureUrl, starts, ends) {

  let startDate = new Date(starts);
  let endDate = new Date(ends);

  let formattedStart = startDate.toLocaleDateString('en-US');
  let formattedEnd = endDate.toLocaleDateString('en-US');

  return `
    <div class="col">
      <div class="card text-center mb-3 ml-3 mr-3 shadow p-3 mb-5 rounded">
        <img src="${pictureUrl}" class="card-img-top">
        <div class="card-body">
          <h5 class="card-title">${name}</h5>
          <h6 class="card-subtitle text-muted">${location}</h6>
          <p class="card-text">${description}</p>
        </div>
        <h5 class="card-date">${formattedStart} - ${formattedEnd}</h5>
      </div>
    </div>
  `;
}

window.addEventListener('DOMContentLoaded', async () => {

    const url = 'http://localhost:8000/api/conferences/';
  
    try {
      const response = await fetch(url);
      if (!response.ok) {
        // Figure out what to do when the response is bad
        alert(`Something went wrong: ${response.status}`);
    } else {
      const data = await response.json();

      for (let conference of data.conferences) {
        const detailUrl = `http://localhost:8000${conference.href}`;
        const detailResponse = await fetch(detailUrl);
        if (detailResponse.ok) {
          const details = await detailResponse.json();
          const name = details.conference.name;
          const description = details.conference.description;
          const pictureUrl = details.conference.location.picture_url;
          const starts = details.conference.starts;
          const ends = details.conference.ends;
          const location = details.conference.location.name;
          const html = createCard(name, location, description, pictureUrl, starts, ends);
          const column = document.querySelector('.row');
          column.innerHTML += html;
          }
          
        }
  
      }
    } catch (e) {
      // Figure out what to do if an error is raised
      alert(`There is an error: ${e.message}`)
    }
  
  });